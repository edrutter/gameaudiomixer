#include "AppComponent.h"

AppComponent::AppComponent()
{
    gui.setAudioObject(&audio);
    addAndMakeVisible(&gui);
    
    for (int i = 0; i < 4; i++)
    {
        gui.addPlayer();
    }
}

AppComponent::~AppComponent()
{

}

//ComponentCallbacks============================================================
void AppComponent::resized()
{
    gui.setBounds(getLocalBounds());
}

//MenuBarCallbacks==============================================================
StringArray AppComponent::getMenuBarNames()
{
	const char* const names[] = { "File", 0 };
	return StringArray (names);
}

PopupMenu AppComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
	PopupMenu menu;
	if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
	return menu;
}

void AppComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
	if (topLevelMenuIndex == FileMenu) 
    {
        if (menuItemID == AudioPrefs) 
        {
            audio.showAudioPreferences(this);
        }
        
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~