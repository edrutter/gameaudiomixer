/*
 Name.h

 Ed Rutter modified 04/01/2013
 */


//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//PublicMembers~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//PrivateMembers-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~

/**Constructor*///==============================================================
//Constructor-------------------------------------------------------------------

/**Destructor*///===============================================================
//Destructor--------------------------------------------------------------------

//MixerSetup====================================================================
//MixerSetup--------------------------------------------------------------------

//Mixer=========================================================================
//Mixer-------------------------------------------------------------------------


//FilePlayer====================================================================
//FilePlayer--------------------------------------------------------------------

//FilePlayerSetup===============================================================
//FilePlayerSetup---------------------------------------------------------------

//TrackControls=================================================================
//TrackControls-----------------------------------------------------------------

//EQControls====================================================================
//EQControls--------------------------------------------------------------------

//AudioSource===================================================================
//AudioSource-------------------------------------------------------------------

//PublicAccessors===============================================================
//PublicAccessors---------------------------------------------------------------

//Application callbacks=========================================================
//Application callbacks---------------------------------------------------------

//ComponentCallbacks============================================================
//ComponentCallbacks------------------------------------------------------------

//ButtonCallback================================================================
//ButtonCallback----------------------------------------------------------------

//SliderCallback================================================================
//SliderCallback----------------------------------------------------------------

//MouseCallbacks================================================================
//MouseCallbacks----------------------------------------------------------------

//FilenameComponentCallback=====================================================
//FilenameComponentCallback-----------------------------------------------------

//Components====================================================================
//Audio=========================================================================
//Variables=====================================================================


//==============================================================================
//------------------------------------------------------------------------------

/*
 When do I need to use shared memory access - SEE NOTES
 Is my use of doubles correct or can I just use floats : float num 10.0     not: float num 10.0f 
 
*/
//ADD AND REMOVE MIXER TRACKS LOOK AT OWNED ARRAY
/*
 • Mixer
    • Add user editable label for tracks that can be accessed on mixer or sampler
    • Level meters
    • USe dB function from Juce Library for sliders http://www.dr-lex.be/info-stuff/volumecontrols.html
    • Send level slider listener
    • Reverse slider colours for Low Pass Filter
    • Shift and Scale filter cutoff controls
 
 • Sampler
    • Interface
        • Play/Pause
        • Stop
        • Pitch slider/dual slider
        • Random pitch on/of button
        • Volume slider/dual slider (subtract from main slider if dual
        • Random volume on/off button
    • Pitch shift array read interpolation      http://stackoverflow.com/questions/5390957/explanation-of-interpolate-hermite-method
 
 • Save/Load mixing settings
    • Create structure or appropriate datatype to store presets between d
    • Morph between settings

 • Impliment Reverb Plugin reverb plugin
 • Write game simulation window
    - Coloured components that read mouse position
        - Zones that trigger samples
        - Zones that morph mixer settings
        - Pan controled by cursor relative to component
 */

/*
 Things I need to show that I know
 Class declaration
    Inheritance
    Polymorphism
    
 Virtual Functions
 
 Threading
 
 Use of new
 
 Different memory locations
 
 */





