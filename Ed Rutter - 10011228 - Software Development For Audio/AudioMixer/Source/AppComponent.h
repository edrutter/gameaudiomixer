#ifndef H_APPCOMPONENT
#define H_APPCOMPONENT

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "GUI.h"
#include "FilePlayerGui.h"

/**
 The main component for the application
 */
class AppComponent  :   public Component,
                        public MenuBarModel
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
	/**Constructor*///==============================================================
	AppComponent();
    
    /**Destructor*///===============================================================
	~AppComponent();
    
	//ComponentCallbacks============================================================
	void resized();
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
	{
		FileMenu=0,
		
		NumMenus
	};
    
    enum FileMenuItems 
	{
        AudioPrefs = 1,
		
		NumFileItems
	};
    StringArray getMenuBarNames();
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName);
	void menuItemSelected (int menuItemID, int topLevelMenuIndex); 

    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    GUI gui;
    
    //Audio=========================================================================
    Audio audio;
};

#endif // H_APPCOMPONENT