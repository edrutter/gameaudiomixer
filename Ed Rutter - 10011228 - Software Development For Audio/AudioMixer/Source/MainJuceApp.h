#ifndef H_JUCEAPP
#define H_JUCEAPP

#include "../JuceLibraryCode/JuceHeader.h"
#include "AppWindow.h"

/**
The top level of the applicaiton 
 */

class JuceApp  : public JUCEApplication
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    JuceApp();
    
    /**Destructor*///===============================================================
    ~JuceApp();
    
    //Applicaiton callbacks=====================================================
    void initialise (const String& commandLine);
    void shutdown();
    //==========================================================================
    void systemRequestedQuit();
    //==========================================================================
    const String getApplicationName();
    const String getApplicationVersion();
    bool moreThanOneInstanceAllowed();
    void anotherInstanceStarted(const String& commandLine);
	
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    AppWindow* appWindow;
    //SamplerWindow* samplerWindow;
};

#endif //H_JUCEAPP