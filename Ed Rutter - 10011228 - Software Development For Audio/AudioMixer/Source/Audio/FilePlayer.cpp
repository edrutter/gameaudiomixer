#include "FilePlayer.h"
//PublicMember-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
//Constructor-------------------------------------------------------------------
FilePlayer::FilePlayer(int arrayNumber) : thread("FilePlayThread")
{
    /*Set up the track number to the number of the array element*/
    trackNumber = arrayNumber;
    
    /*Create a new resampling audio source and sets a pointer to it.
     It's input source is then set to the audio transport source
     */
    resamplingAudioSource = new ResamplingAudioSource(&transportSource, false);
    
    /*Create a series of new audio source filter and set pointers to them.
     The first input is set to to the resampling audio source and the following filter's
     are set to the previous filter in a chain.
     */
    filterAudioSource[0] = new IIRFilterAudioSource(resamplingAudioSource, false);
    for (int iBand = 1; iBand < 7; iBand++)
    {
        filterAudioSource[iBand] = new IIRFilterAudioSource(filterAudioSource[iBand - 1], false);
    }
    thread.startThread();
}

//Destructor--------------------------------------------------------------------
FilePlayer::~FilePlayer()
{
    transportSource.setSource(nullptr);// unload the current file
    thread.stopThread(100); //stop thread after pausing
}

//MixerSetup--------------------------------------------------------------------
void FilePlayer::setMixer(MixerPlusAudioSource* mixer_)
{
    mixer = mixer_;
}

void FilePlayer::setAuxMixer(int auxNumber, MixerPlusAudioSource* mixer_)
{
    jassert(auxNumber >= 0)
    auxMixer[auxNumber] = mixer_;
}

int FilePlayer::getTrackNumber()
{
    return trackNumber.get();
}

//FilePlayer--------------------------------------------------------------------
void FilePlayer::loadFile (const File& newFile)
{
    // unload the previous file source and delete it..
    setPlaying(false);
    transportSource.setSource (0);
    currentAudioFileSource = nullptr;
    
    // create a new file source from the file..
    // get a format manager and set it up with the basic types (wav, ogg and aiff).
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    AudioFormatReader* reader = formatManager.createReaderFor (newFile);
    
    if (reader != 0)
    {
        //currentFile = audioFile;
        currentAudioFileSource = new AudioFormatReaderSource (reader, true);
        
        // ..and plug it into our transport source
        transportSource.setSource (currentAudioFileSource,
                                   32768, // tells it to buffer this many samples ahead
                                   &thread,
                                   reader->sampleRate);
    }
}

void FilePlayer::setPlaying (const bool newState)
{
    if(newState == true)
    {
        transportSource.setPosition(playbackPosition.get());
        transportSource.start();
    }
    else
    {
        transportSource.stop();
    }
}

bool FilePlayer::isPlaying () const
{
    return transportSource.isPlaying();
}

void FilePlayer::setPosition (float newPosition)
{
    jassert(newPosition >= 0.0 && newPosition <= 1.0)
    /*Sets the playback position of the transport source*/
    playbackPosition = newPosition * transportSource.getLengthInSeconds();
    transportSource.setPosition(playbackPosition.get());
}

float FilePlayer::getPosition()
{
    /*Gets the playback position, a value between 0.0 and 1.0*/
    if (transportSource.getLengthInSeconds() > 0.0)
    {
    return transportSource.getCurrentPosition() / transportSource.getLengthInSeconds();
    }
    else
    {
        return 0.0;
    }
}

void FilePlayer::setPlaybackRate (float newRate)
{
    jassert(newRate > 0 && newRate < 2)
    /*Set the resampling audio sources resampling ratio changing the pitch up with values over 1.0 and down with values below*/
    resamplingAudioSource->setResamplingRatio(newRate);
}

//TrackControls-----------------------------------------------------------------
void FilePlayer::setTrackGain (const float newGain)
{
    jassert(newGain >= 0.0)
    mixer->setInputGain(trackNumber.get(), newGain);
}

void FilePlayer::setTrackPan (const float newPan)
{
    jassert(newPan >= 0.0 && newPan <= 1.0)
    mixer->setInputPan(trackNumber.get(), newPan);
}

void FilePlayer::setTrackMuteState(int state)
{
    mixer->setInputMute(trackNumber.get(), state);
}

void FilePlayer::setTrackSoloState(int state)
{
    jassert(state >= 0.0 && state <= 1.0)
    
    trackSoloState = state;
    if (state == 1)
    {
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
    }
    else if (state == 1)
    {
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////
    }
}

void FilePlayer::setAuxLevel (int auxNumber, const float auxLevel)
{
    auxMixer[auxNumber]->setInputGain(trackNumber.get(), auxLevel);
}

//EQControls--------------------------------------------------------------------
void FilePlayer::setBandCutoff(int band, double newCutoff)
{
    jassert(newCutoff >= 20.0 && newCutoff <= 20000.0)
    trackCandCutoff[band] = newCutoff;
    filterParameterSetup(band);
}

void FilePlayer::setBandQ(int band, double newQ)
{
    jassert(newQ >= 0.0)
    trackBandQ[band] = newQ;
    filterParameterSetup(band);
}

void FilePlayer::setBandGain(int band, double newGain)
{
    jassert(newGain >= 0.0 && newGain <= 1.0)
    trackBandGain[band] = newGain;
    filterParameterSetup(band);
}

void FilePlayer::setBandState(int band, bool state)
{
    if (state == true)
    {
        filterParameterSetup(band);
    }
    else
    {
        IIRFilter filterSetup;
        filterSetup.makeBandPass(sampleRate.get(), 20000, 0.001, 1.0);
        filterAudioSource[band]->setFilterParameters(filterSetup);
    }
}

//AudioSource-------------------------------------------------------------------
void FilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{    
    filterAudioSource[6]->prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void FilePlayer::releaseResources()
{
    resamplingAudioSource->releaseResources();
    
    for (int iBand = 0; iBand < 7; iBand++)
    {
    filterAudioSource[iBand]->releaseResources();
    }
}

void FilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    filterAudioSource[6]->getNextAudioBlock(bufferToFill);
}

//PrivateMembers-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
void FilePlayer::filterParameterSetup(int band)
{
    jassert(band >= 0)
    IIRFilter filterSetup;
    
    switch (band)
    {
        case 0:
            filterSetup.makeHighShelf(sampleRate.get(), trackCandCutoff[band].get(), trackBandQ[band].get(), trackBandGain[band].get());
            break;
            
        case 1: case 2: case 3:
            filterSetup.makeBandPass(sampleRate.get(), trackCandCutoff[band].get(), trackBandQ[band].get(), trackBandGain[band].get());
            break;
            
        case 4:
            filterSetup.makeLowShelf(sampleRate.get(), trackCandCutoff[band].get(), trackBandQ[band].get(), trackBandGain[band].get());
            break;
            
        case 5:
            filterSetup.makeHighPass(sampleRate.get(), trackCandCutoff[band].get());
            break;
            
        case 6:
            filterSetup.makeLowPass(sampleRate.get(), trackCandCutoff[band].get());
            break;
    }
    filterAudioSource[band]->setFilterParameters(filterSetup);
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~