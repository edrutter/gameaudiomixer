#ifndef H_FILEPLAYER
#define H_FILEPLAYER

#include "../JuceLibraryCode/JuceHeader.h"
#include "MixerPlusAudioSource.h"

/**
 A file player 
 */
class FilePlayer :  public AudioSource
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    FilePlayer(int arrayNumber);
    
    /**Destructor*///===============================================================
    ~FilePlayer();
    
    //MixerSetup====================================================================
    /**
     Sets the main track mixer
     
     @param mixer_ pointer to a mixer
     */
    void setMixer(MixerPlusAudioSource* mixer_);
    
    /**
     Sets the aux mixers
     
     @param auxNumber auxillary bus number
     
     @param mixer_ pointer to a mixer
     */
    void setAuxMixer(int auxNumber, MixerPlusAudioSource* mixer_);
    
    /**
     Gets the current track number
     
     @return track number (mixer input number)
     */
    int getTrackNumber();
    
    //FilePlayer====================================================================
    /**
     Loads the specified file into the transport source
     
     @param newFile address of a file for the transport source to play
     */
    void loadFile(const File& newFile);
    
    /**
     Tells the player to play or stop
     
     @param newState    true: play
                        false: stop playback
     */
    void setPlaying(const bool newState);
    
    /**
     Gets the current playback state of the looper
     
     @return    true: is playing
                false: is stopped
     */
    bool isPlaying() const;
    
    /**
     Set play position
     
     @param newPosition sets the new playback position
     */
    void setPosition (float newPosition);
    
    /**
     Gets the play position
     
     @return current position
     */
    float getPosition();

    /**
     Set playback rate or pitch
     
     @param newPitchRatio   if set to 1.0, the input is passed through; higher
                            values will speed it up; lower values will slow it
                            down. The ratio must be greater than 0
     */
    void setPlaybackRate (float newRate);
    
    //TrackControls=================================================================
    /**
     Sets the gain of the fileplayer 
     
     @param newGain         if set to 1.0 audio is played at original level
                            values above one will amplify the audio source
                            values below will attenuate the source
                            Range 0 - ∞
     */
    void setTrackGain (const float newGain);
    
    /**
     Sets the pan of the fileplayer
     
     @param newPan         if set to 0.5 audio is played equally on the left
     and right channels 0.0 plays only on left, 1.0 plays only on right
     Range 0.0 - 1.0
     */
    void setTrackPan (const float newPan);

    /**
     Set the tracks mute state. If the track is muted it's audio will not be
     heard.
     
     @param state   true: track is muted
                    false: track is not muted
     */
    void setTrackMuteState(int state);
    
    /**
     Set the tracks solo state
     if one or more tracks are soloed
     they will be the only tracks playing
     
     @param state   true: track is soloed
                    false: track is not soloed
     */
    void setTrackSoloState(int state);
    
    /**
     Sets the mixers aux send level
     
     @param auxNumber auxillary bus number
     
     @param auxLevel auxillary send level range 0.0 - 1.0
     */
    void setAuxLevel (int auxNumber, const float auxLevel);

    //EQControls====================================================================
    /**
     Sets the cutoff for an EQ band
     
     @param band filter band to set
     
     @param newCutoff new cutoff value range 20 - 20,000
     */
    void setBandCutoff (int band, double newCutoff);
    
    /**
     Sets the Q for an EQ band
     
     @param filter band to set Q value
     
     @param newQ new Q value
     
     */
    void setBandQ (int band, double newQ);
    
    /**
     Sets the cutoff frequency for an EQ band
     
     @param bfilter band to set cuttof frequency
     
     @param newCutoff new cutoff value
     */
    void setBandGain (int band, double newGain);
    
    /**
     Turns an EQ band on or off by setting values that will not alter the sound.
     
     @param filter band to turn on/off
     
     @param state   true: on 
                    false: off
     
     */
    void setBandState(int band, bool state);

    //AudioSource===================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate);
    void releaseResources();
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
    
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //FilterSetup===================================================================
    /*
     Set all parameters of filter band
     
     @param band EQ band to set up
     */
    void filterParameterSetup(int band);
    
    //Audio=========================================================================
    ScopedPointer <AudioFormatReaderSource> currentAudioFileSource; //reads audio from the file
    AudioTransportSource transportSource;	// this controls the playback of a positionable audio stream, handling the starting/stopping and sample-rate conversion
    ScopedPointer <ResamplingAudioSource> resamplingAudioSource; //provides pitch control of audio source
    
    ScopedPointer <IIRFilterAudioSource> filterAudioSource[7]; //provides control of frequency content of audio source
    
    TimeSliceThread thread;     //thread for the transport source
    
    MixerPlusAudioSource* mixer;           // pointer to main track mixer
    MixerPlusAudioSource* auxMixer[3];     // pointer to aux bus mixers
    
    //Variables=====================================================================
    Atomic<double> sampleRate = 44100;
    
    Atomic<double> playbackPosition = 0;

    Atomic<double> trackGain;
    Atomic<double> trackPan;
    Atomic<int> trackSoloState;
    Atomic<int> trackMuteState;
    Atomic<int> trackNumber;
    
    Atomic<int> trackBandState[7];
    Atomic<double> trackCandCutoff[7];
    Atomic<double> trackBandQ[5] = {0.001, 0.001, 0.001, 0.001, 0.001};
    Atomic<double> trackBandGain[5] = {1.0, 1.0, 1.0, 1.0, 1.0};
};
#endif  // H_FILEPLAYER