#ifndef H_AUDIO
#define H_AUDIO

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"
#include "MixerPlusAudioSource.h"

/**
 Contains high level audio objects. This is where a vector array of file players is
 created, mixed and streamed to the audio device.
 */
class Audio  : public AudioIODeviceCallback
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
	/**Constructor*///==============================================================
	Audio();
    
    /**Destructor*///===============================================================
	~Audio();
    
    /**
     Adds an audio player, prepare it to play, set its output to the audio source 
     mixer and returns a pointer to it
     
     @return A pointer to the new fileplaver
     */
    FilePlayer* addPlayer();
    
    /**
     Removes the last audio player and removes it from the mixer's input
     */
    void removePlayer();
    
    /**
     Shows the audio prefs window
     
     @param centerComponent A pointer to a component to centre around
     */
    void showAudioPreferences(Component* centerComponent);
    

	//==============================================================================
    //AudioIO
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples);
    
    void audioDeviceAboutToStart (AudioIODevice* device);
    
    void audioDeviceStopped();
  
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Audio=========================================================================
    AudioDeviceManager audioDeviceManager; // this wraps the actual audio device
    AudioSourcePlayer audioSourcePlayer;   // this allows an audio source to be streamed to the IO device
    
    MixerPlusAudioSource outputMixer;  // mixes audio output bus (auxes and main mix)
    MixerPlusAudioSource trackMixer;   // mixes audio tracks
    MixerPlusAudioSource auxMixer[3];  // mix auxillary bus sends
    ScopedPointer <ReverbAudioSource> auxReverb[2];

    OwnedArray<FilePlayer> filePlayer;  //audio file players
    
    CriticalSection sharedMemory;
    
    //Variables=====================================================================
    int samplesPerBlock;
    double sampleRate;
};

#endif //H_AUDIO