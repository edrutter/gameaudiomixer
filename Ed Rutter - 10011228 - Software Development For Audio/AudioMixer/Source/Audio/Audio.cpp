#include "Audio.h"

Audio::Audio()
{  
    samplesPerBlock = -1;   //uninitialised state
    // and initialise the device manager with no settings so that it picks a
    // default device to use.
    const String error (audioDeviceManager.initialise (1, /* number of input channels */
                                                       2, /* number of output channels */
                                                       0, /* no XML settings.. */
                                                       true  /* select default device on failure */));
    
    if (error.isNotEmpty())
    {
        AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                    "sdaAudioMix",
                                     "Couldn't open an output device!\n\n" + error);
    }
    else
    {
        /*Set */
        audioSourcePlayer.setSource (&outputMixer);
        
        /*Start audio IO device pulling audio  from our callback*/
        audioDeviceManager.addAudioCallback (this);
    }
    
    for (int auxNum = 0; auxNum < 2; auxNum++)
    {
    /*Create a ReverbAudioSource and set it's input to the relevant Aux bus*/
    auxReverb[auxNum] = new ReverbAudioSource(&auxMixer[auxNum], false);
    
    /*Create a Reverb::Parameter structure and set test values*/
        Reverb::Parameters param;
        if (auxNum == 1) param.roomSize = 1.0;
        param.wetLevel = 1.0;
        param.dryLevel = 0.0;
    
    /*Use Reverb::Parameter to set ReverbAudioSource parameters.
     Prepare ReverbAudioSource to play*/
    auxReverb[auxNum]->setParameters(param);
    auxReverb[auxNum]->prepareToPlay(samplesPerBlock, sampleRate);
    }
    
    /*Set output mixer's audio sources*/
    /*ONLY ONE AUDIO SOURCE CAN BE USED AT A TIME HERE OR BUFFER PROBLEMS OCCUR*/
    outputMixer.addInputSource(&trackMixer, false);
//    outputMixer.addInputSource(&auxMixer[0], false);
//    outputMixer.addInputSource(&auxMixer[10], false);
//    outputMixer.addInputSource(auxReverb[0], false);
//    outputMixer.addInputSource(auxReverb[1], false);
}

Audio::~Audio()
{
    /*Unregister audio callback, remove all mixer inputs and set source player's source to a null pointer*/
    audioDeviceManager.removeAudioCallback (this);//unregister the audio callback
    trackMixer.removeAllInputs();
    for (int auxNum = 0; auxNum < 2; auxNum++)
    {
        auxMixer[auxNum].removeAllInputs();
    }
    
    outputMixer.removeAllInputs();
    audioSourcePlayer.setSource(nullptr);
}

void Audio::showAudioPreferences(Component* centerComponent)
{
    AudioDeviceSelectorComponent audioSettingsComp (audioDeviceManager,
                                                    0, 2, 2, 2,
                                                    true, true, true, true);
    audioSettingsComp.setSize (500, 250);
    DialogWindow::showModalDialog ("Audio Settings", &audioSettingsComp, centerComponent, Colours::lightgrey, true);
}


FilePlayer* Audio::addPlayer()
{
    /*Add a new element to the pointer array of fileplayers and set the new fileplayer*/
    filePlayer.add(new FilePlayer(filePlayer.size()));
    
    if (samplesPerBlock != -1)
    {
        /*prepare fileplayers to play and set mixer to control them*/
        filePlayer.getLast()->prepareToPlay(samplesPerBlock, sampleRate);
        filePlayer.getLast()->setMixer(&trackMixer);
        for (int auxNum = 0; auxNum < 2; auxNum++)
        {
            filePlayer.getLast()->setAuxMixer(auxNum, &auxMixer[auxNum]);
        }
    }
    trackMixer.addInputSource(filePlayer.getLast(), false);
    for (int auxNum = 0; auxNum < 2; auxNum++)
    {
        auxMixer[auxNum].addInputSource(filePlayer.getLast(), false);
    }
    return filePlayer.getLast();
}

void Audio::removePlayer()
{
    if (filePlayer.size() > 0) 
    {
        trackMixer.removeInputSource(filePlayer.getLast());
        auxMixer[0].removeInputSource(filePlayer.getLast());
        filePlayer.removeLast();
    }
}

//==============================================================================
//AudioIO
void Audio::audioDeviceIOCallback ( const float** inputChannelData,
                                    int numInputChannels,
                                    float** outputChannelData,
                                    int numOutputChannels,
                                    int numSamples)
{
    audioSourcePlayer.audioDeviceIOCallback(inputChannelData, 
                                            numInputChannels, 
                                            outputChannelData, 
                                            numOutputChannels, 
                                            numSamples);
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart(device);
    samplesPerBlock = device->getCurrentBufferSizeSamples();
    sampleRate = device->getCurrentSampleRate();
}

void Audio::audioDeviceStopped()
{
    samplesPerBlock = -1;   //uninitialised state
    audioSourcePlayer.audioDeviceStopped();
}