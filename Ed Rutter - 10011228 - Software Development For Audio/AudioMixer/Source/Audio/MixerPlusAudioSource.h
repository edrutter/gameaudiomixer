//
//  MixerPlus.h
//  sdaAudioMix
//
//  Created by Ed Rutter Legacy on 19/03/2013.
//
//

#ifndef H_MIXER_PLUS
#define H_MIXER_PLUS

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class MixerPlusAudioSource : public MixerAudioSource
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    MixerPlusAudioSource();
    
    //Mixer=========================================================================
    /** Adds an input source to the mixer.
     
     If the mixer is running you'll need to make sure that the input source
     is ready to play by calling its prepareToPlay() method before adding it.
     If the mixer is stopped, then its input sources will be automatically
     prepared when the mixer's prepareToPlay() method is called.
     
     @param newInput             the source to add to the mixer
     @param deleteWhenRemoved    if true, then this source will be deleted when
     no longer needed by the mixer.
     */
    void addInputSource (AudioSource* newInput, bool deleteWhenRemoved);
    
    /** Removes an input source.
     If the source was added by calling addInputSource() with the deleteWhenRemoved
     flag set, it will be deleted by this method.
     */
    void removeInputSource (AudioSource* input);
  
    
    /** Implementation of the AudioSource method. */
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill);
    
    //TrackControls=================================================================
    /** Sets the gain value for the input
     
     @param inputNumber input number to modify
     
     @param gain Value to multiply the input by
     */
    void setInputGain (int inputNumber, float gain);
    
    /** Sets the pan value for the input
     
     @param inputNumber input number to modify
     
     @param pan Pans the audio left (values below 0.5) or right (values over 0.5)
     */
    void setInputPan (int inputNumber, float pan);
    
    /** Sets the solo state for the input
     
     @param inputNumber input number to modify
     
     @param state   true: track is soloed
     false: track is not soloed
     */
    void setInputMute (int inputNumber, bool state);
    
    /** Sets the mute state for the input
     
     @param inputNumber input number to modify
     
     @param state   true: track is muted
                    false: track is not muted
     */
    void setInputSolo (int inputNumber, bool state);
 
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //==============================================================================
    BigInteger inputsToDelete;
    CriticalSection lock;
    
    //Audio=========================================================================
    AudioSampleBuffer tempBuffer;
    
    //Variables=====================================================================
    Array <AudioSource*> inputs;    //pointer to audio source input
    Array <double> gainValue;       //gain value of input
    Array <double> panValue;        //pan value of input
    Array <bool> soloState;         //solo state of input
    Array <bool> muteState;         //mute state of input
    Array <int> inputNumber;
    
    double currentSampleRate;
    int bufferSizeExpected;
};


#endif /* defined(H_MIXER_PLUS) */
