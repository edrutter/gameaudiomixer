//
//  MixerPlus.cpp
//  sdaAudioMix
//
//  Created by Ed Rutter Legacy on 19/03/2013.
//
//

#include "MixerPlusAudioSource.h"
//PrivateMembers-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
//Constructor-------------------------------------------------------------------
MixerPlusAudioSource::MixerPlusAudioSource() :    tempBuffer (2, 0),
currentSampleRate (0.0),
bufferSizeExpected (0)
{
    
}

//Destructor--------------------------------------------------------------------


//Mixer-------------------------------------------------------------------------
void MixerPlusAudioSource::addInputSource (AudioSource* input, const bool deleteWhenRemoved)
{
    if (input != nullptr && ! inputs.contains (input))
    {
        double localRate;
        int localBufferSize;
        
        {
            const ScopedLock sl (lock);
            localRate = currentSampleRate;
            localBufferSize = bufferSizeExpected;
        }
        
        if (localRate > 0.0)
            input->prepareToPlay (localBufferSize, localRate);
        
        const ScopedLock sl (lock);
        
        inputsToDelete.setBit (inputs.size(), deleteWhenRemoved);
        inputs.add (input);
        inputNumber.add(0);
        gainValue.add(1.0);
        panValue.add(0.5);
        soloState.add(false);
        muteState.add(false);
        
        inputs.getLast();
    }
}

void MixerPlusAudioSource::removeInputSource (AudioSource* const input)
{
    if (input != nullptr)
    {
        ScopedPointer<AudioSource> toDelete;
        
        {
            const ScopedLock sl (lock);
            const int index = inputs.indexOf (input);
            
            if (index < 0)
                return;
            
            if (inputsToDelete [index])
                toDelete = input;
            
            inputsToDelete.shiftBits (-1, index);
            inputs.remove (index);
            inputNumber.remove(index);
            gainValue.remove(index);
            soloState.remove(index);
            muteState.remove(index);
        }
        
        input->releaseResources();
    }
}

void MixerPlusAudioSource::getNextAudioBlock (const AudioSourceChannelInfo& info)
{
    const ScopedLock sl (lock);
    
    if (inputs.size() > 0)
    {
        /*Get sample data from input 0 and store it in info buffer*/
        inputs.getUnchecked(0)->getNextAudioBlock (info);
        
        /*set temp buffer size and create an AudioSourceChannelInfo to
         wrap the buffer*/
        tempBuffer.setSize (jmax (1, info.buffer->getNumChannels()),
                            info.buffer->getNumSamples());
        AudioSourceChannelInfo info2 (&tempBuffer, 0, info.numSamples);
        
        
        /*For first input check if track is muted. If not cycle through
         all channels applying gain and pan*/
        if (muteState.getUnchecked(0) == false)
        {
            for (int chan = 0; chan < info.buffer->getNumChannels();
                 chan++)
            {
                if (chan%2 == 0) //Left Channel
                {
                    info.buffer->applyGain(chan, 0,
                                           info.buffer->getNumSamples(), gainValue.getUnchecked(0) * (1.0 -
                                                                                                      panValue.getUnchecked(0)));
                }
                else if (chan%2 == 1) //Right Channel
                {
                    info.buffer->applyGain(chan, 0,
                                           info.buffer->getNumSamples(), gainValue.getUnchecked(0) *
                                           panValue.getUnchecked(0));
                }
            }
        }
        else
        {
            for (int chan = 0; chan < info.buffer->getNumChannels();
                 chan++)
            {
                info.buffer->applyGain(chan, 0,
                                       info.buffer->getNumSamples(), 0.f);
            }
        }
        
        /*For all other inputs cycle through inputs getting sample data
         from input and storing it in info2.*/
        if (inputs.size() > 1)
        {
            for (int i = 1; i < inputs.size(); ++i)
            {
                inputs.getUnchecked(i)->getNextAudioBlock (info2);
                
                /*Check if tracks are muted. If not apply channel gain
                 and pan.*/
                if (muteState.getUnchecked(i) == false)
                {
                    for (int chan = 0; chan <
                         info.buffer->getNumChannels(); chan++)
                    {
                        if (chan%2 == 0) //Left Channel
                        {
                            info2.buffer->applyGain(chan, 0,
                                                    info.buffer->getNumSamples(), gainValue.getUnchecked(i) * (1.0 -
                                                                                                               panValue.getUnchecked(i)));
                        }
                        else if (chan%2 == 1) // Right Channel
                        {
                            info2.buffer->applyGain(chan, 0,
                                                    info.buffer->getNumSamples(), gainValue.getUnchecked(i) *
                                                    panValue.getUnchecked(i));
                            
                        }
                        
                        /*
                         Add data from tempBuffer to info (original
                         buffer passed in).
                         
                         I SUSPECT THIS MAY BE WHERE THE PROBLEM LIES.
                         I HAVE TRIED USING COPYFROM INSTEAD OF ADDFROM 
                         BUT THIS JUST PRODUCEDS SILENCE.
                         
                         I THINK IT MAY BE DUE WITH ALL INPUTS POINTING 
                         TO THE ONE AUDIO SOURCE CHANNEL INFO
                         
                         MAY BE A +1 OR -1 NEEDED SOMEWHERE?????
                         */
                    }
                    for (int chan = 0; chan < 
                         info.buffer->getNumChannels(); ++chan)
                    {
                        info.buffer->addFrom (chan, info.startSample, 
                                              tempBuffer, chan, 0, info.numSamples);
                    }
                }
                
                
            }
        }
    }
    else
    {
        info.clearActiveBufferRegion();
    }
}


//TrackControls-----------------------------------------------------------------
void MixerPlusAudioSource::setInputGain (int inputNumber, float gain)
{
    jassert(gain >= 0.0)
    gainValue.set(inputNumber, gain);
}

void MixerPlusAudioSource::setInputPan (int inputNumber, float pan)
{
    jassert(pan >= 0.0 && pan <= 1.0)
    panValue.set(inputNumber, pan);
}

void MixerPlusAudioSource::setInputMute (int inputNumber, bool state)
{
    muteState.set(inputNumber, state);
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~