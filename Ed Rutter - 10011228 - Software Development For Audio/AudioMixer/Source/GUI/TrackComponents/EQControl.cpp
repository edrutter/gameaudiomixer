/*
 EQ.cpp
 
 Ed Rutter modified 10/01/2013
 */

#include "EQControl.h"

//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//Constructor-------------------------------------------------------------------
EQControls::EQControls()
{
    /*Setup Cutoff rotary slider and label*/
    addAndMakeVisible(&cutoffSlider);
    cutoffSlider.setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);
    cutoffSlider.setRange(0, 1);
    cutoffSlider.setValue(0.5);
    cutoffSlider.addListener(this);
    cutoffSlider.setEnabled(state);
    
    addAndMakeVisible(&cutoffLabel);
    cutoffLabel.setText("Hz", true);
    cutoffLabel.setColour(cutoffLabel.textColourId, Colours::lightgrey);
    
    /*Setup Q rotary slider and label*/
    addAndMakeVisible(&qSlider);
    qSlider.setRange(0.001, 1.0, 0.001);
    qSlider.setValue(0.5);
    qSlider.setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);
    qSlider.addListener(this);
    qSlider.setEnabled(state);
    
    addAndMakeVisible(&qLabel);
    qLabel.setText("Q", true);
    qLabel.setColour(qLabel.textColourId, Colours::lightgrey);
    
    /*Setup Gain rotary slider and label*/
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(-0.999, 0.999, 0.001);
    gainSlider.setValue(0);
    gainSlider.setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);
    gainSlider.addListener(this);
    gainSlider.setEnabled(state);
    
    addAndMakeVisible(&gainLabel);
    gainLabel.setText("dB", true);
    gainLabel.setJustificationType(Justification::right);
    gainLabel.setColour(gainLabel.textColourId, Colours::lightgrey);
    
    /*Setup On/Off Button*/
    addAndMakeVisible(&onOffButton);
    onOffButton.setColour(onOffButton.buttonColourId, Colours::white);
    onOffButton.addListener(this);
    
    /*Setup Band label*/
    addAndMakeVisible(&bandLabel);
    bandLabel.setColour(bandLabel.textColourId, Colours::darkgrey);
    bandLabel.setJustificationType(Justification::horizontallyCentred);
}


//Destructor--------------------------------------------------------------------
EQControls::~EQControls()
{
    
}

//PublicAccessors---------------------------------------------------------------

void EQControls::setUpEQs(int band)
{
    bandNumber = band;
    
    switch (band)
    {
        case 0:
            bandLabel.setText("High Shelf", true);
            cutoffSlider.setValue(0);
            break;
        case 1:
            bandLabel.setText("High", true);
            cutoffSlider.setValue(0.75);
            break;
        case 2:
            bandLabel.setText("Mid", true);
            cutoffSlider.setValue(0.5);
            break;
        case 3:
            bandLabel.setText("Low", true);
            cutoffSlider.setValue(0.25);
            break;
        case 4:
            bandLabel.setText("Low Shelf", true);
        default:
            break;
    }
}

//ComponentCallbacks------------------------------------------------------------
void EQControls::resized()
{
    int iW = getWidth();
    int iH = getHeight();
    int iRotSize = 30;
    
    cutoffSlider.setBounds  (0,                     0,                      iRotSize,       iRotSize);
    gainSlider.setBounds    (iW - iRotSize,         iRotSize - 3,           iRotSize,       iRotSize);
    qSlider.setBounds       (0,                     (iRotSize - 3) * 2,     iRotSize,       iRotSize);
    
    cutoffLabel.setBounds   (iRotSize,              0,                      iRotSize,       iRotSize);
    gainLabel.setBounds     (iW - iRotSize *2,      iRotSize - 3,           iRotSize,       iRotSize);
    qLabel.setBounds        (iRotSize,              (iRotSize - 3) * 2,     iRotSize,       iRotSize);
    
    bandLabel.setBounds     (0,                     iH - 22,                iW - 11,        iW * 0.4);
    onOffButton.setBounds   (iW - (iW * 0.2 + 2),   iH - (iW * 0.2 + 2),    iW * 0.2,       iW * 0.2);
    
}

void EQControls::paint (Graphics &g)
{
    g.setColour(Colours::whitesmoke);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 3);
}


//ButtonCallback---------------------------------------------------------------
void EQControls::buttonClicked(Button* button)
{
    if (state == true)
    {
        state = false;
        onOffButton.setColour(onOffButton.buttonColourId, Colours::white);
        qLabel.setColour(qLabel.textColourId, Colours::lightgrey);
        cutoffLabel.setColour(cutoffLabel.textColourId, Colours::lightgrey);
        gainLabel.setColour(gainLabel.textColourId, Colours::lightgrey);
        bandLabel.setColour(bandLabel.textColourId, Colours::darkgrey);
    }
    else
    {
        state = true;
        onOffButton.setColour(onOffButton.buttonColourId, Colours::indianred);
        qLabel.setColour(qLabel.textColourId, Colours::darkgrey);
        cutoffLabel.setColour(cutoffLabel.textColourId, Colours::darkgrey);
        gainLabel.setColour(gainLabel.textColourId, Colours::darkgrey);
        bandLabel.setColour(bandLabel.textColourId, Colours::black);
    }
    
    filePlayer->setBandState(bandNumber, state);
    
    cutoffSlider.setEnabled(state);
    gainSlider.setEnabled(state);
    qSlider.setEnabled(state);
}


//SliderCallback----------------------------------------------------------------
void EQControls::sliderValueChanged(Slider* slider)
{
    if (slider == &cutoffSlider)
    {
        /*Set */
        cutoffValue = cutoffSlider.getValue();
        cutoffValue = cutoffValue * cutoffValue* cutoffValue;
        cutoffValue = (cutoffValue * 19980) + 20;
        
        filePlayer->setBandCutoff(bandNumber, cutoffValue);
    }
    else if (slider == &gainSlider)
    {
        gainValue = gainSlider.getValue();
        
        if (gainValue > 0)
        {
            gainValue = gainValue * gainValue * gainValue;
        }
        else if (gainValue < 0)
        {
            gainValue = gainValue * gainValue * gainValue * -1;
        }
        
        gainValue = gainValue + 1;
        
        filePlayer->setBandGain(bandNumber, gainValue);
        
    }
    else if (slider == &qSlider)
    {
        qValue = qSlider.getValue();
        
        filePlayer->setBandQ(bandNumber, qValue);
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~