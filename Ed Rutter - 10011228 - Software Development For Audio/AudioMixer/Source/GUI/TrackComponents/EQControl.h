/*
 EQ.h
 
 Ed Rutter modified 10/01/2013.
 */

#ifndef H_EQ_CONTROLS
#define H_EQ_CONTROLS

#include "RotarySlider.h"
#include "FilePlayerControlComponent.h"

class EQControls  :     public FilePlayerControlComponent,
                        public ButtonListener,
                        public Slider::Listener
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
	EQControls();
    
    /**Destructor*///===============================================================
	~EQControls();
    
    //EQSetup=======================================================================
    /**
     Sets the band labels and intialises slider values
     
     @param band EQ band to set up
     */
    void setUpEQs(int band);
    
    //ComponentCallbacks============================================================
    void resized();
    void paint (Graphics &g);
    
    //ButtonCallback================================================================
    void buttonClicked(Button* button);
    
    //SliderCallback================================================================
    void sliderValueChanged(Slider* slider);
    
    
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    RotarySlider cutoffSlider;
    RotarySlider qSlider;
    RotarySlider gainSlider;
    TextButton onOffButton;
    
    Label cutoffLabel;
    Label qLabel;
    Label gainLabel;
    Label bandLabel;
    
    //Variables=====================================================================
    double cutoffValue = 20;
    double gainValue = 0;
    double qValue = 1;
    
    int bandNumber;
    bool state = false;
};

#endif // H_EQ_CONTROLS