/*
 MixerTrack.h
 
 Ed Rutter modified 10/01/2013.
 */

#ifndef H_MIXER_TRACK
#define H_MIXER_TRACK

#include "TrackControls.h"
#include "EQControl.h"
#include "FilterControls.h"
#include "SendControls.h"
#include "FilePlayerControlComponent.h"
#include "MixerPlusAudioSource.h"

class MixerTrackGUI    :    public FilePlayerControlComponent
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    MixerTrackGUI();
    
    /**Destructor*///===============================================================
    ~MixerTrackGUI();
    
    //Setup=========================================================================
    void setMixer(MixerPlusAudioSource* mixer_);
    
    /**
     Sets the file player for the controls to control
     
     @param filePlayer_ A pointer to a file player
     */
    void setFilePlayer (FilePlayer* filePlayer_);

    //ComponentCallbacks============================================================
    void resized();
    
    
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    TrackControls control;
    EQControls  eQ[5];
    FilterControls filter[2];
    SendControls sends;
    
    MixerPlusAudioSource* mixer;
};

#endif // H_MIXER_TRACK