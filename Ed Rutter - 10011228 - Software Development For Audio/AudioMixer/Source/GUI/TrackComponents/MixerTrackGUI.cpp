/*
 MixerTrack.cpp
 
 Ed Rutter Modified 10/01/2013
 */

#include "MixerTrackGUI.h"

//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//Constructor-------------------------------------------------------------------
MixerTrackGUI::MixerTrackGUI()
{
    addAndMakeVisible(&sends);
    addAndMakeVisible(&control);
    for (int index = 0; index < 2; index++)
    {
        addAndMakeVisible(&filter[index]);
        filter[index].setUpFilter(index);
    }
    for (int iBand = 0; iBand < 5; iBand++)
    {
        addAndMakeVisible(&eQ[iBand]);
    }
}

//Destructor--------------------------------------------------------------------
MixerTrackGUI::~MixerTrackGUI()
{
    
}

void MixerTrackGUI::setMixer(MixerPlusAudioSource* mixer_)
{
    mixer = mixer_;
//    control.setMixer(mixer);
}

//PublicAccessors---------------------------------------------------------------
void MixerTrackGUI::setFilePlayer (FilePlayer* filePlayer_)
{
    filePlayer = filePlayer_;
    
    control.setFilePlayer(filePlayer);
    sends.setFilePlayer(filePlayer);
    
    for (int iBand = 0; iBand < 5; iBand++)
    {
        if (iBand < 2)
        {
            filter[iBand].setFilePlayer(filePlayer);
        }
        eQ[iBand].setFilePlayer(filePlayer);
    }
}

//ComponentCallbacks------------------------------------------------------------
void MixerTrackGUI::resized()
{
    int iW = getWidth();    //track width
    int iH = getHeight();   //track height
    float fMH = 1.0/10.0;   //Module height
    
    /*Set size of modules on mixer track from top to the bottom*/
    sends.setBounds         (0,     iH * fMH * 0,               iW,     iH * fMH - 5);
    filter[0].setBounds     (0,     iH * fMH * 1,               iW,     iH * fMH * 0.5 - 5);
    
    for (int iBand = 0; iBand < 5; iBand++)
    {
        eQ[iBand].setBounds (0,     iH * fMH * (iBand + 1.5),   iW,     iH * fMH - 5);
        eQ[iBand].setUpEQs(iBand);
    }
    
    filter[1].setBounds     (0,     iH * fMH * 6.5,             iW,     iH * fMH * 0.5 - 5);
    control.setBounds       (0,     iH * fMH * 7,               iW,     iH * fMH * 2);
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
