/*
 MixerSendControls.h
 
 Ed Rutter modified 10/01/2013.
 */

#ifndef H_SEND_CONTROLS
#define H_SEND_CONTROLS

#include "FilePlayerControlComponent.h"
#include "RotarySlider.h"
#include "FilePlayer.h"

#define NUMBER_OF_SENDS 3

class SendControls  :   public FilePlayerControlComponent,
                        public Slider::Listener
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    SendControls();
    
    /**Destructor*///===============================================================
    ~SendControls();
    
    //ComponentCallbacks============================================================
    void resized();
    void paint(Graphics &g);
    
    //SliderCallback================================================================
    void sliderValueChanged(Slider* slider);
    
    
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    RotarySlider send[NUMBER_OF_SENDS];
    Label sendLabel[4];
    
    //Variables=====================================================================
    double aux[NUMBER_OF_SENDS];
};

#endif //H_SEND_CONTROLS

