/*
 MixerSlider.h
 
 Ed Rutter modified 10/01/2013
 */

#ifndef H_TRACK_CONTROLS
#define H_TRACK_CONTROLS

#include "RotarySlider.h"
#include "FilePlayerControlComponent.h"
#include "MixerPlusAudioSource.h"

/**
 
 
 */

class TrackControls  :  public FilePlayerControlComponent,
                        public ButtonListener,
                        public SliderListener,
                        public ActionBroadcaster
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    TrackControls();
    
    /**Destructor*///===============================================================
    ~TrackControls();

    //ComponentCallbacks============================================================
    void resized();
    void paint (Graphics &g);
    
    //ButtonCallbacks===============================================================
    void buttonClicked(Button* button);
    
    //SliderCallbacks===============================================================
    void sliderValueChanged(Slider* slider);
	
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    RotarySlider trackGain;
    RotarySlider trackVolume;
    RotarySlider trackPan;
    TextButton trackSolo;
    TextButton trackMute;

    //MixerPlusAudioSource* mixer;
    //Variables=====================================================================
    bool soloState;
    bool muteState;
    
    double gainValue;
    double panValue;
    double volumeValue;
};

#endif // H_TRACK_CONTROLS