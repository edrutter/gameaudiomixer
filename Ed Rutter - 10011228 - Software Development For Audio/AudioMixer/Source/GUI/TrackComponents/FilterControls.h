/*
 FilterControls.h
 
 A component class which including a rotary slider, on/off button and labels.
 Designed to be used with fixed slope high and low pass filters
 
 Ed Rutter modified 10/01/2013.
 */

#ifndef H_FILTER_CONTROLS
#define H_FILTER_CONTROLS

#include "RotarySlider.h"
#include "FilePlayerControlComponent.h"

class FilterControls  : public FilePlayerControlComponent,
                        public ButtonListener,
                        public Slider::Listener,
                        public ActionBroadcaster
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
	FilterControls();
    
    /**Destructor*///===============================================================
	~FilterControls();
    
    //PublicAccessors===============================================================
    /**
     Sets the file player for the controls to control
     
     @param filePlayer_ A pointer to a file player
     */
    void setFilePlayer (FilePlayer* filePlayer_);
    
    /**
     Set filter label and default slider value
     
     @param band filter band to set up
     */
    void setUpFilter(int band);
	
    //ComponentCallbacks============================================================
    void resized();
    void paint (Graphics &g);
    
    //ButtonCallback================================================================
    void buttonClicked(Button* button);
    
    //SliderCallback================================================================
    void sliderValueChanged(Slider* slider);
    
	
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    RotarySlider cutoffSlider; //Slider for controlling filter frequencies
    TextButton onOffButton;   //Button for switching filter on and off
    Label cutoffLabel;
    Label label;
    
    FilePlayer* filePlayer;  
    
    //Variables=====================================================================
    int bandNumber;
    double cutoffValue;
    bool state = false;
};

#endif // H_FILTER_CONTROLS