/*
 Track.cpp
 
 Ed Rutter modified 10/01/2013
 */

#include "TrackControls.h"

//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//Constructor-------------------------------------------------------------------
TrackControls::TrackControls()
{
    
    
    /*Initialise variables*/
    soloState = false;
    muteState = false;
    gainValue = 0;
    panValue = 0;
    volumeValue = 0;
    
    /*Volume control slider setup*/
    addAndMakeVisible(&trackVolume);
    trackVolume.setSliderStyle(Slider::LinearVertical);
    trackVolume.setRange(0, 1.33, 0.000001);  // check this range
    trackVolume.setValue(1);
    trackVolume.addListener(this);
    
    /*Pan/balance contror slider setup*/
    addAndMakeVisible(&trackPan);
    trackPan.setRange(0, 1, 0.0001);
    trackPan.setValue(0.5);
    trackPan.addListener(this);
    
    /*Gain control setup slider*/
    addAndMakeVisible(&trackGain);
    trackGain.setRange(0.5, 1.8, 0.0001);    //is range suitable
    trackGain.setValue(1);
    trackGain.addListener(this);
    
    /*Solo button setup*/
    addAndMakeVisible(&trackSolo);
    trackSolo.setColour(trackMute.buttonColourId, Colours::white);
    trackSolo.addListener(this);
    
    /*Mute button setup*/
    addAndMakeVisible(&trackMute);
    trackMute.setColour(trackMute.buttonColourId, Colours::white);
    trackMute.addListener(this);
}

TrackControls::~TrackControls()
{
    
}

//void TrackControls::setMixer(MixerPlus* mixer_)
//{
//    mixer = mixer_;
//}

//ComponentCallbacks------------------------------------------------------------
void TrackControls::resized()
{
    int iW = getWidth();
    int iH = getHeight();
    
    /*Set component bounds relative to size of component*/
    /*                      x position      y position      width           height             */
    trackSolo.setBounds     (iW * 0.2 - 2,  iH * 0.02,      iW * 0.2 + 2,   iW * 0.2 + 2);
    trackMute.setBounds     (iW * 0.6,      iH * 0.02,      iW * 0.2 + 2,   iW * 0.2 + 2);
    
    trackGain.setBounds     (iW * 0.2,      iW * 0.4,       iW * 0.6,       iW * 0.6);
    trackPan.setBounds      (iW * 0.2,      iW - 5,         iW * 0.6,       iW * 0.6);
    trackVolume.setBounds   (0,             iW * 1.6 - 10,  iW * 0.6,       iH - (iW * 1.6 - 10));
}

void TrackControls::paint(Graphics &g)
{
    g.setColour(Colours::whitesmoke);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 3);
}


//ButtonCallbacks---------------------------------------------------------------
void TrackControls::buttonClicked(Button* button)
{
    /*Set solo state and button colour if solo button is clicked*/
    if (button == &trackSolo)
    {
        if (soloState == true)
        {
            soloState = false;
            trackSolo.setColour(trackSolo.buttonColourId, Colours::white);
        }
        else
        {
            soloState = true;
            trackSolo.setColour((trackSolo.buttonColourId), Colours::darkgreen);
        }
    }
    
    /*Set mute state and button colour if mute button is clicked*/
    else if (button == &trackMute)
    {
        if (muteState == true)
        {
            muteState = false;
            trackMute.setColour(trackMute.buttonColourId, Colours::white);
        }
        else
        {
            muteState = true;
            trackMute.setColour((trackMute.buttonColourId), Colours::darkred);
        }
        filePlayer->setTrackMuteState(muteState);
    }
}


//SliderCallbacks---------------------------------------------------------------
void TrackControls::sliderValueChanged(Slider* slider)
{
    /*Get slider value, convert to approximated dB scale, set gain variable, set file player gain*/
    if (slider == &trackGain)
    {
        gainValue = trackGain.getValue();
        gainValue = gainValue * gainValue * gainValue * gainValue;
        
        filePlayer->setTrackGain(gainValue * volumeValue);
    }
    
    /*Get slider value, convert to approximated dB scale, set volume variable, set file player gain*/
    else if (slider == &trackVolume)
    {
        volumeValue = trackVolume.getValue();
        volumeValue = volumeValue * volumeValue * volumeValue * volumeValue;
        filePlayer->setTrackGain(gainValue * volumeValue);
    }
    
    /*Set pan variable if pan slider is moved*/
    else if (slider == &trackPan)
    {
        panValue = trackPan.getValue();
        filePlayer->setTrackPan(panValue);
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~