/*
 MixerSendControls.cpp
 
 Ed Rutter modified 10/01/2013
 */

#include "SendControls.h"

//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//Constructor-------------------------------------------------------------------
SendControls::SendControls()
{
    for (int iAux = 0; iAux < NUMBER_OF_SENDS; iAux++)
    {
        addAndMakeVisible(&send[iAux]);
        addAndMakeVisible(&sendLabel[iAux]);
        send[iAux].setRange(0.0, 1.0);
        send[iAux].addListener(this);
    }
    
    addAndMakeVisible(&sendLabel[3]);
    sendLabel[0].setText("Aux 1", true);
    sendLabel[1].setText("Aux 2", true);
    sendLabel[2].setText("LFE", true);
    sendLabel[3].setText("Sends", true);
}

SendControls::~SendControls()
{
    
}

//ComponentCallbacks------------------------------------------------------------
void SendControls::resized()
{
    int iW = getWidth();
    int iH = getHeight();
    int iRotSize = 30;
    
    for (int bus = 0; bus < NUMBER_OF_SENDS; bus++)
    {
        send[bus].setBounds   (0, (iRotSize - 3) * bus , iRotSize, iRotSize);
        sendLabel[bus].setBounds(30, (iRotSize - 3) * bus , iW - 30, iRotSize);
        
        if (bus == NUMBER_OF_SENDS -1)
        {
            sendLabel[3].setBounds(0, iH - 22, iW - 10, iW * 0.4);
        }
    }
}

void SendControls::paint (Graphics &graffix)
{
    graffix.setColour(Colours::whitesmoke);
    graffix.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 3);
}

//SliderCallback----------------------------------------------------------------
void SendControls::sliderValueChanged(Slider* slider)
{
    
    for (int bus = 0; bus < NUMBER_OF_SENDS; bus++)
    {
        if (slider == &send[bus])
        {
            aux[bus] = send[bus].getValue();
            filePlayer->setAuxLevel(bus, aux[bus]);
        }
        
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~