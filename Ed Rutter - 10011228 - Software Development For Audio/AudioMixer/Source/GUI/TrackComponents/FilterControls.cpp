/*
 FilterControls.cpp
 
 Ed Rutter modified 10/01/2013
 */

#include "FilterControls.h"

//PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
//=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~

//Constructor-------------------------------------------------------------------
FilterControls::FilterControls()
{
    addAndMakeVisible(&cutoffSlider);
    cutoffSlider.setRange(0, 1);
    cutoffSlider.addListener(this);
    cutoffSlider.setEnabled(state);
    
    addAndMakeVisible(&onOffButton);
    onOffButton.setColour(onOffButton.buttonColourId, Colours::white);
    onOffButton.addListener(this);
    
    addAndMakeVisible(&cutoffLabel);
    cutoffLabel.setColour(cutoffLabel.textColourId, Colours::lightgrey);
    cutoffLabel.setText("Hz", true);
    
    
    addAndMakeVisible(&label);
    label.setColour(label.textColourId, Colours::darkgrey);
    label.setJustificationType(Justification::horizontallyCentred);
}

//Destructor--------------------------------------------------------------------
FilterControls::~FilterControls()
{
    
}

//PublicAccessors---------------------------------------------------------------

void FilterControls::setFilePlayer (FilePlayer* filePlayer_)
{
    filePlayer = filePlayer_;
}

void FilterControls::setUpFilter(int band)
{
    bandNumber = band + 5;
    
    switch (band)
    {
        case 0:
            label.setText("High Pass", true);
            cutoffSlider.setValue(0);
            break;
        case 1:
            label.setText("Low Pass", true);
            cutoffSlider.setValue(1);
            break;
        default:
            break;
    }
}

//ComponentCallbacks------------------------------------------------------------
void FilterControls::resized()
{
    int iW = getWidth();
    int iH = getHeight();
    int iRotSize = 30;
    
    cutoffSlider.setBounds        (0,                     0,                      iRotSize,       iRotSize);
    
    cutoffLabel.setBounds   (iRotSize - 5,          0,                      iRotSize,       iRotSize);
    
    label.setBounds         (0,                     iH - 22,                iW - 11,        iW * 0.4);
    onOffButton.setBounds         (iW - (iW * 0.2 + 2),   iH - (iW * 0.2 + 2),    iW * 0.2,       iW * 0.2);
    
}

void FilterControls::paint (Graphics &graffix)
{
    graffix.setColour(Colours::whitesmoke);
    graffix.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 3);
}

//ButtonCallback----------------------------------------------------------------
void FilterControls::buttonClicked(Button* button)
{
    if (state == true)
    {
        state = false;
        onOffButton.setColour(onOffButton.buttonColourId, Colours::white);
        cutoffLabel.setColour(cutoffLabel.textColourId, Colours::lightgrey);
        label.setColour(label.textColourId, Colours::darkgrey);
    }
    else
    {
        state = true;
        onOffButton.setColour(onOffButton.buttonColourId, Colours::indianred);
        cutoffLabel.setColour(cutoffLabel.textColourId, Colours::darkgrey);
        label.setColour(label.textColourId, Colours::black);
    }
    
    filePlayer->setBandState(bandNumber, state);
    cutoffSlider.setEnabled(state);
}

//SliderCallback----------------------------------------------------------------
void FilterControls::sliderValueChanged(Slider* slider)
{
    cutoffValue = cutoffSlider.getValue();
    cutoffValue = cutoffValue * cutoffValue* cutoffValue * cutoffValue;
    cutoffValue = (cutoffValue * 19980) + 20;
    
    filePlayer->setBandCutoff(bandNumber, cutoffValue);
}

//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~