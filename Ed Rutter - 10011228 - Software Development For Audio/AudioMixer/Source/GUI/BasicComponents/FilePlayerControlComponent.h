//
//  FilePlayerControlComponent.h
//  sdaAudioMix
//
//  Created by Ed Rutter Legacy on 18/03/2013.
//
//

#ifndef H_FILE_Player_Control_COMPONENT
#define H_FILE_Player_Control_COMPONENT

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

/*
 A simple class which enables a fileplayer pointer to a fileplayer to be stored
 */

class FilePlayerControlComponent    :   public Component
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**
     Sets the file player for the controls to control
     
     @param filePlayer_ Pointer to a file player
     */
    void setFilePlayer (FilePlayer* filePlayer_);
    
    //ProtectedMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
protected:
    //Components====================================================================
    FilePlayer* filePlayer;
};


#endif /* defined(H_FILE_Player_Control_COMPONENT) */
