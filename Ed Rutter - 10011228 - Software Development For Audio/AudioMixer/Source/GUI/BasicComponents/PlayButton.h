/*
 Ed Rutter
 */
#ifndef H_PLAY_BUTTON
#define H_PLAY_BUTTON

class PlayButton : public TextButton 
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    PlayButton() : TextButton("u", "click here to play the current audio file")
    {
        setColour(TextButton::buttonColourId, Colours::darkgrey);
    }
    Font 	getFont (){return Font ("Wingdings 3", jmin(getHeight(),getWidth())/1.5f, Font::plain);}
};

#endif //H_PLAY_BUTTON