/*
 AudioWaveformThumbnail.h
 Ed Rutter
 19/03/2013
 */

#ifndef H_AUDIO_WAVEFORM_THUMBNAIL
#define H_AUDIO_WAVEFORM_THUMBNAIL

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

/*
 Implements the JUCE audio thumbnail to allow a waveform 
 representation to be visually displayed on the user interface.
 It all allows setting of the current play position by clicking
 on the waveform.
 */

class AudioWaveformThumbnail :  public Component,
                                public ChangeListener
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    AudioWaveformThumbnail();
    
    /**Destructor*///===============================================================
    ~AudioWaveformThumbnail();
    
    //FilePlayerSetup===============================================================
    /**
     Sets the source, size and file player for the thumbnail to control.
     
     @param fileForThumb An audio file for the thumbnail to display
     
     @param filePlayer_ A pointer to a file player
     */
    void setFilePLayer(File fileForThumb, FilePlayer* filePlayer_);
    
    //ComponentCallbacks============================================================
    /*
     The x position of the mouse click is used to set the file player's
     playback position if a file is loaded.
     */
    void mouseDown (const MouseEvent& event);

    /*
     Draws the waveform on the interface if a file is loaded
     */
    void paint(Graphics &g);
    //ChangeListenerCallback========================================================
    void changeListenerCallback(ChangeBroadcaster* source);
    
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Audio=========================================================================
    AudioFormatManager audioFormatManager;
    AudioThumbnail audioThumbnail;
    AudioThumbnailCache audioThumbnailCache;
    
    FilePlayer* fileplayer; //pointer to the file player to create a thumbnail for
    
    MouseEvent* event;      //pointer to a mouse event to temporaril

    //Variables=====================================================================
    float startTime;
    float endTime;
};

#endif /* defined(H_AUDIO_WAVEFORM_THUMBNAIL) */
