/*
 Ed Rutter
 */
#include "AudioWaveformThumbnail.h"

AudioWaveformThumbnail::AudioWaveformThumbnail() :
audioThumbnail(512, audioFormatManager, audioThumbnailCache),
audioThumbnailCache(1)
{
    startTime = 0;
    endTime = 0;
    
    audioThumbnail.addChangeListener(this);
    audioFormatManager.registerBasicFormats();
}

AudioWaveformThumbnail::~AudioWaveformThumbnail()
{
    audioThumbnail.removeChangeListener(this);
}

void AudioWaveformThumbnail::setFilePLayer(File fileForThumb, FilePlayer* filePlayer_)
{
    audioThumbnail.setSource(new FileInputSource(fileForThumb));
    startTime = 0;
    endTime = audioThumbnail.getTotalLength();
    fileplayer = filePlayer_;
    repaint();
}

void AudioWaveformThumbnail::paint(Graphics &g)
{
    g.setColour(Colours::whitesmoke);
    
    if (endTime > 0)
    {
        audioThumbnail.drawChannel(g, getLocalBounds(), startTime, endTime, 1, 0.8);
    }
}

void AudioWaveformThumbnail::changeListenerCallback(ChangeBroadcaster* source)
{
    repaint();
}

void AudioWaveformThumbnail::mouseDown(const MouseEvent& event)
{
    if (endTime >0)
    {
        float xPos = event.x;
        float width = getWidth();
        fileplayer->setPosition(xPos/width);
        repaint();
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~