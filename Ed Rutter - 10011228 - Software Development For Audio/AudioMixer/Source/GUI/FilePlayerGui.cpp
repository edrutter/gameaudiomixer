#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui ()
{
    addAndMakeVisible(&audioWaveformThumbnail);
    
    filePlayer = nullptr;
    
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    pitch.addListener(this);
    pitch.setRange(0.1, 2);
    pitch.setValue(1);
    pitch.setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);
    addAndMakeVisible(&pitch);
    
//    gain.addListener(this);
//    gain.setRange(0, 1);
//    gain.setValue(1);
//    gain.setTextBoxStyle(Slider::NoTextBox, 0, 0, 0);
//    addAndMakeVisible(&gain);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
}

//Destructor--------------------------------------------------------------------
FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}

//ComponentCallbacks------------------------------------------------------------
void FilePlayerGui::resized()
{
    int iRotSize = getHeight();
    
    playButton.setBounds                (3,                         3, iRotSize - 6,                iRotSize - 6);
    audioWaveformThumbnail.setBounds    (iRotSize,                  0, getWidth() - (iRotSize * 7), iRotSize);
    pitch.setBounds                     (getWidth() - iRotSize * 6, 0, iRotSize,                    iRotSize);
//    gain.setBounds                      (getWidth() - iRotSize,     0, iRotSize,                    iRotSize);
    fileChooser->setBounds              (getWidth() - iRotSize * 5, 5, iRotSize * 5,                iRotSize - 10);
}


//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        if (filePlayer != nullptr)
        {
            filePlayer->setPlaying(!filePlayer->isPlaying());
            if(filePlayer->isPlaying())
            {
                startTimer(10);
                playButton.setColour(playButton.textColourOffId, Colours::darkred);

            }
            else
            {
                stopTimer();
                playButton.setColour(playButton.textColourOffId, Colours::black);

            }
        }
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            if (filePlayer != nullptr)
                filePlayer->loadFile(audioFile);
            audioWaveformThumbnail.setFilePLayer(audioFile, filePlayer);
            
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

//Slider Listener
void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &pitch)
    {
        if (filePlayer != nullptr)
            filePlayer->setPlaybackRate(pitch.getValue());
    }
//    else if (slider == &gain)
//    {
//        if (filePlayer != nullptr)
//        {
//            filePlayer->setTrackGain(gain.getValue());
//        }
//    }
}

//Timer
void FilePlayerGui::timerCallback()
{
    if (filePlayer != nullptr)
    {
        repaint();
    }
}

void FilePlayerGui::paint(Graphics &g)
{
        playbackPositionValue = int(filePlayer->getPosition() * (getWidth() - (getHeight() * 7)));

        g.setColour(Colours::darkgrey);
        g.fillRoundedRectangle(playbackPositionValue + getHeight(), 2, 2, getHeight() - 4 , 2);
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
