//  SamplePlayersGUI.h
//  Created by Ed Rutter Legacy on 12/03/2013.
//
//

#ifndef H_SAMPLE_PLAYER_GUI
#define H_SAMPLE_PLAYER_GUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "FilePlayerGui.h"

class SamplePlayerGUI   :   public Component
{
public:
    /**Constructor*///==============================================================
    SamplePlayerGUI();
   
    /**Destructor*///===============================================================
    ~SamplePlayerGUI();
    
    //Setup=========================================================================
    /**
     Set audio object
     */
    void setAudioObject(Audio* audioObject);
    
    /**
     Adds a player to the GUI
     
     @param newFilePLayer pointer to the new file player
     */
    void addPlayer(FilePlayer* newFilePlayer);
    
    /**
     Removes a player from the GUI
     */
    void removePlayer();
    
    //Component callbacks=====================================================
    void resized();

    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    Array<FilePlayerGui*> fileTrackGUI;
    Audio* audio;
};

#endif /* defined(H_SAMPLE_PLAYER_GUI) */
