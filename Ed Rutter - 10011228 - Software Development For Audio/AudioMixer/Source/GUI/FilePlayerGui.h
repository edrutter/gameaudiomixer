//
//  FilePlayerGUI.h
//  sdaFileEffect
//
//  Created by Ed Rutter Legacy on 26/02/2013.
//
//

#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayerControlComponent.h"
#include "PlayButton.h"
#include "RotarySlider.h"
#include "AudioWaveformThumbnail.h"

/**
 Gui for the FilePlayer class this needs to have a file player set 
 */
class FilePlayerGui :   public FilePlayerControlComponent,
                        public Button::Listener,
                        public Slider::Listener,
                        public FilenameComponentListener,
                        public Timer
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:    
    /**Constructor*///==============================================================
    FilePlayerGui();
    
    /**Destructor*///===============================================================
    ~FilePlayerGui();
    
    //ButtonCallback================================================================
    void buttonClicked (Button* button);

    //SliderCallback================================================================
    void sliderValueChanged (Slider* slider);
    
    //FilenameComponentCallback=====================================================
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged);
    
    //TimerCallback=================================================================
    void timerCallback();
    
    //ComponentCallbacks============================================================
    void resized();
    void paint(Graphics &g);

    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
    
    //Components====================================================================
    PlayButton playButton;
    RotarySlider pitch;
//    RotarySlider gain;
    
    FilenameComponent* fileChooser;
    
    AudioWaveformThumbnail audioWaveformThumbnail;
    
    //Variables=====================================================================
    int playbackPositionValue;
};


#endif  // H_FILEPLAYERGUI
