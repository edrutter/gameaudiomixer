//  MixerGUI.h
//
//  Created by Ed Rutter Legacy on 12/03/2013.
//
//

#ifndef H_MIXER_GUI
#define H_MIXER_GUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "MixerTrackGUI.h"

class MixerClassGUI : public Component
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    MixerClassGUI();
    
    /**Destructor*///===============================================================
    ~MixerClassGUI();
    
    //MixerSetup====================================================================
    /**
     Sets the audio object for the
     
     @param audioObject pointer to the audio object to be controlled
     */
    void setAudioObject(Audio* audioObject);
    
    /**
     Adds a player to the GUI
     
     @param newFilePLayer pointer to the new file player
     */
    void addPlayer(FilePlayer* newFilePlayer);
    
    /**
     Removes a player from the GUI
     */
    void removePlayer();
    
    //Component callbacks=====================================================
    void resized();

    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    Array<MixerTrackGUI*> mixerTrackGUI;
 
    //Audio=========================================================================
    Audio* audio;
};
#endif /* defined(H_MIXER_GUI) */
