#include "Gui.h"

GUI::GUI() : audio(nullptr)
{
    /*Create and set up a tabbed*/
    tabbedComponent = new TabbedComponent(TabbedButtonBar::TabsAtTop);
    addAndMakeVisible(tabbedComponent);
	tabbedComponent->setTabBarDepth(25);
    tabbedComponent->addTab("Mixer", Colours::lightgrey, &mixerObject, false);
    tabbedComponent->addTab("Sample Player", Colours::lightgrey, &samplerPlayerObject, false);
    
    /*Create and set up add and remove track buttons*/
    addButton.setButtonText("+");
    addButton.setConnectedEdges(Button::ConnectedOnLeft);
    addButton.setColour(TextButton::buttonColourId, Colours::lightgrey);
    addButton.addListener(this);
    addAndMakeVisible(&addButton);
    
    removeButton.setButtonText("-");
    removeButton.setConnectedEdges(Button::ConnectedOnRight);
    removeButton.setColour(TextButton::buttonColourId, Colours::lightgrey);
    removeButton.addListener(this);
    addAndMakeVisible(&removeButton);
}

GUI::~GUI()
{
    delete tabbedComponent;
}

void GUI::setAudioObject(Audio* audioObject)
{
    if (audioObject != nullptr)
    {
        audio = audioObject;
    }
}

void GUI::addPlayer()
{
    //Adds a file player and creates a pointer to it
    FilePlayer* newFilePlayer = audio->addPlayer();
    
    /*Passes the links new player to in mixer and sample player GUIs*/
    mixerObject.addPlayer(newFilePlayer);
    samplerPlayerObject.addPlayer(newFilePlayer);
    
    resized();
    numberOfplayers++;
}

void GUI::removePlayer()
{
    //remove a file player
    if (numberOfplayers > 0) // NEED TO KEEP TRACK OF TRACK COUNT ELSEWHERE
    {
        mixerObject.removePlayer();
        samplerPlayerObject.removePlayer();
        audio->removePlayer();
    }
    resized();
    numberOfplayers--;
}

//Component callbacks=====================================================
void GUI::resized()
{
    tabbedComponent->setBounds(5, 0, getWidth() - 5, getHeight() - 5);    
    addButton.setBounds(getWidth()-53, 0, 50, 20);
    removeButton.setBounds(getWidth()-103, 0,50,20);
}

void GUI::buttonClicked(Button* button)
{
    if (button == &addButton)
    {
        addPlayer();
    }
    else if (button == &removeButton)
    {
        removePlayer();
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~