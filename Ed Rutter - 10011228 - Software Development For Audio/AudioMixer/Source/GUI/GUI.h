#ifndef H_GUI
#define H_GUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayerGui.h"
#include "MixerTrackGUI.h"
#include "SamplePlayersGUI.h"
#include "Audio.h"
#include "MixerGUI.h"

/**
 GUI component
 */

class GUI  :    public Component,
                public Button::Listener
{
    //PublicMembers=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
public:
    /**Constructor*///==============================================================
    GUI();
    
    /**Destructor*///===============================================================
    ~GUI();
    
    /**
     Sets the audio object for the
     
     @param audioObject the audio object to be controlled
     */
    void setAudioObject(Audio* audioObject);
    
    /**
     Adds a player to the GUI
     */
    void addPlayer();
    
    /**
     Removes a player from the GUI
     */
    void removePlayer();
    
    //Component callbacks=====================================================
    void resized();
    
    //Button callback=========================================================
    void buttonClicked(Button* button);
	
    //PrivateMembers~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
private:
    //Components====================================================================
    TabbedComponent *tabbedComponent;
    MixerClassGUI mixerObject;
    SamplePlayerGUI samplerPlayerObject;
    Array<FilePlayerGui*> filePlayerGuis;
    
    TextButton addButton;
    TextButton removeButton;
    
    //Audio=========================================================================
    Audio* audio;
    
    //Variables=====================================================================
    int numberOfplayers = 0;
};

#endif //H_GUI