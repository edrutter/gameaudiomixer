//
//  SamplePlayersGUI.cpp
//  sdaAudioMix
//
//  Created by Ed Rutter Legacy on 12/03/2013.
//
//

#include "SamplePlayersGUI.h"

SamplePlayerGUI::SamplePlayerGUI()
{
    
}

SamplePlayerGUI::~SamplePlayerGUI()
{
    for (int i = fileTrackGUI.size()-1; i >= 0; i--)
    {
        delete fileTrackGUI[i];
    }
    fileTrackGUI.clear();

}

void SamplePlayerGUI::setAudioObject(Audio* audioObject)
{
    if (audioObject != nullptr)
    {
        audio = audioObject;
    }
}

void SamplePlayerGUI::addPlayer(FilePlayer* newFilePlayer)
{
    fileTrackGUI.add(new FilePlayerGui());
    fileTrackGUI.getLast()->setFilePlayer(newFilePlayer);
    addAndMakeVisible(fileTrackGUI.getLast());
    
    resized();
}

void SamplePlayerGUI::removePlayer()
{
    removeChildComponent(fileTrackGUI.getLast());
    delete fileTrackGUI.getLast();
    fileTrackGUI.removeLast();
    resized();
}


void SamplePlayerGUI::resized()
{
    int size = fileTrackGUI.size();
    
    int height = 40;
    
    for (int i = 0; i < size; i++)
    {
        fileTrackGUI[i]->setBounds(5,      5 + (height * i),   getWidth() - 10,    height);
    }
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~