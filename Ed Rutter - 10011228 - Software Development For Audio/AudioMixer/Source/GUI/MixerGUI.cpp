//
//  MixerClassGUI.cpp
//  sdaAudioMix
//
//  Created by Ed Rutter Legacy on 12/03/2013.
//
//

#include "MixerGUI.h"

MixerClassGUI::MixerClassGUI() :    audio(nullptr)
{
//    addAndMakeVisible(&currentParameter);
}

MixerClassGUI::~MixerClassGUI()
{
    for (int i = mixerTrackGUI.size()-1; i >= 0; i--)
    {
        delete mixerTrackGUI[i];
    }
    mixerTrackGUI.clear();
}

void MixerClassGUI::setAudioObject(Audio* audioObject)
{
    if (audioObject != nullptr)
    {
        audio = audioObject;
    }
}

void MixerClassGUI::addPlayer(FilePlayer* newFilePlayer)
{
    mixerTrackGUI.add(new MixerTrackGUI());
    mixerTrackGUI.getLast()->setFilePlayer(newFilePlayer);
    addAndMakeVisible(mixerTrackGUI.getLast());
    
    resized();
}


void MixerClassGUI::removePlayer()
{
    removeChildComponent(mixerTrackGUI.getLast());
    delete mixerTrackGUI.getLast();
    mixerTrackGUI.removeLast();
    
    resized();
}


void MixerClassGUI::resized()
{
    int size = mixerTrackGUI.size();
    
    for (int i = 0; i < size; i++)
    {
        mixerTrackGUI[i]->setBounds (5 + (80 * i),  5,  75, 1000);
    }
    
//    currentParameter.setBounds(0, getHeight() - 25, 50, 20);
}
//-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~